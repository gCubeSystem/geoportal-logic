This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.application.geoportal-logic

## [v1.0.15-SNAPSHOT] - 2021-07-29
Exposed PostgisDB connection descriptor

## [v1.0.14] - 2020-12-11
Fixes "no transaction" issue


## [v1.0.13] - 2020-12-11
Geoserver filename clash
Updated model concessioni


## [v1.0.12] - 2020-12-11
Fixed missing centroid

## [v1.0.11] - 2020-12-11
Fixed wms url
Fixed duplicate posizionamento

## [v1.0.10] - 2020-12-11
Fixed filenames

## [v1.0.9] - 2020-12-9
Exclude gcube 5 libraries

## [v1.0.8] - 2020-12-9
Restrict gis dependencies version

## [v1.0.7] - 2020-12-7
Concessione : Automatic coordinates from posizionamento scavo

## [v1.0.6-SNAPSHOT] - 2020-12-4
Fix layer coordinates
Geoserver folder management

## [v1.0.5] - 2020-12-1

Deletion feature
Fixed Layer Management

## [v1.0.4] - 2020-11-9
Profiles adoption
Fixed collection management
Fixed Serialization
Fixed 


## [v1.0.3-SNAPSHOT] - 2020-11-4

Implemented list, list by type
Fixed : commit transaction when safelyCommit project
Fixed : exception encapsulation in DB initialization

## [v1.0.2] - 2020-11-4

PublicationReport
Fix style publication [File not found]
Fix getManagerByID


## [v1.0.1] - 2020-11-2
SDI publication exception are now non blocking
Automatic centroids layer creation
Default EntityManagerFactory to be set by caller

## [v1.0.0] - 2020-09-25

First release