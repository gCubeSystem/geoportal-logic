package org.gcube.application.geoportal;

import java.io.File;
import java.util.Map;

import org.gcube.application.geoportal.model.Configuration;
import org.gcube.application.geoportal.model.db.DatabaseConnection;
import org.gcube.application.geoportal.storage.PostgisDBManager;
import org.gcube.application.geoportal.utils.MainUtils;
import org.gcube.spatial.data.geonetwork.GeoNetworkPublisher;
import org.gcube.spatial.data.gis.GISInterface;

public class Geoportal {

	private static final String USAGE="<CMD> <OPTIONS>"
			+ "CMD : upload, cleanup, list"
			+ "options : "
			+ "--UPLOAD_TYPE : CONCESSIONI | TEMPLATE | GPKG_TABLE"
			+ "--DBUSER"
			+ "--DBPWD"
			+ "--DBURL"
			+ "--CSVFile"
			+ "--CONTEXT"
			+ "--SHALLOW"
			+ "--REMOVE_EXISTENT"
			+ "--TABLENAME";
			
	
	public static void main(String[] args) throws Exception {
		if(args.length<1) {
			System.out.println(USAGE);
			System.exit(0);
		}
		
		String cmd=args[0];

		Map<String,String> options=MainUtils.asMap(args);
		System.out.println("CMD : "+cmd);
		System.out.println("Options : "+options);
		
//		ArchiveManager archiveManager=new ArchiveManagerImpl();
		
//		TokenSetter.set(MainUtils.getMandatory("CONTEXT", options));
		
		
//		String workspace = "geona-proto";
//		String storeName = "postgis_db";
		// TEST Config
		Configuration config=new Configuration(true,"preventiva-test","concessioni-test","gna-test_archeomar","postgis_db");
		
		//PROD CONFIG
		config=new Configuration(false,"","concessioni","","postgis_db");
		
		
		switch(cmd) {
		
		case "upload":{
			
			
			
			GeoNetworkPublisher geonetwork=null;
			GISInterface gis=null;
			DatabaseConnection postgisConnection= new DatabaseConnection(MainUtils.getMandatory("DBUSER",options),
					MainUtils.getMandatory("DBPWD",options), MainUtils.getMandatory("DBURL",options));
			
			postgisConnection.setAutocommit(false);
			
			PostgisDBManager.init(postgisConnection);
			
;
			
//			postgisConnection.setAutocommit(true);
			Uploader uploader=null;
//					new Uploader(gis,geonetwork,archiveManager,config);
			
			Boolean centroidsOnly=Boolean.parseBoolean(MainUtils.getOptional("SHALLOW", options, "true"));
			Boolean removeExistent=Boolean.parseBoolean(MainUtils.getOptional("REMOVE_EXISTENT", options, "true"));
			
			config.setCleanUpBeforeUpdate(removeExistent);
			
			switch(MainUtils.getMandatory("UPLOAD_TYPE", options)) {
			case "CONCESSIONI" : {
				uploader.insertConcessioni(new File(MainUtils.getMandatory("CSVFile",options)),centroidsOnly);
				break;
			}
			
			case "TEMPLATE":{
				String tablename=MainUtils.getOptional("TABLENAME",options,null);
//				String layerTitle=MainUtils.getOptional("LAYERNAME",options,tablename);
				uploader.insertMOSIFromTemplate(MainUtils.getMandatory("GPKG",options), 
						tablename, centroidsOnly);
				break;
			}
			
			case "GPKG_TABLE" :{
				String tablename=MainUtils.getMandatory("TABLENAME",options);
//				uploader.publishGpkgFeature(MainUtils.getMandatory("GPKG",options), 
//						tablename);
				break;
				
			}
			}
			
			
			
			break;
		}
		
		}
		
		
	}
		
	
}
