package org.gcube.application.geoportal;


import org.gcube.application.geoportal.managers.ManagerFactory;
import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.application.geoportal.model.fault.SDIInteractionException;
import org.gcube.application.geoportal.storage.SDIManager;
import org.gcube.application.geoportal.storage.WorkspaceManager;
import org.gcube.application.geoportal.utils.ISUtils;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.Item;
import org.gcube.data.transfer.library.faults.RemoteServiceException;
import org.gcube.data.transfer.model.RemoteFileDescriptor;

public class CheckContextConfiguration {

	public static void main(String[] args) throws ConfigurationException, StorageHubException, SDIInteractionException, RemoteServiceException {
//		TokenSetter.set("/gcube/devNext/NextNext");		
//		TokenSetter.set("/pred4s/preprod/preVRE");
		
		TokenSetter.set("/d4science.research-infrastructures.eu/D4OS/GeoNA-Prototype");
		
//		System.out.println("Checking for internal .. ");
//		ConcessioneManager manager=ManagerFactory.getByRecord(new Concessione());
//		try {
//			manager.commit(false);
//		} catch (ValidationException | PersistenceException | PublishException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}finally {
//			manager.shutdown();
//		}
		
		
		// Checking token validity
		// only from 1.0.3
//		System.out.println("Checking for token ");
//		String token = ISUtils.getToken();
//		System.out.println("Token Found ");
//		
//		String toRestore= (SecurityTokenProvider.instance.get());
//		System.out.println("Current Identity : "+ScopeUtils.getCurrentCaller());
//		SecurityTokenProvider.instance.set(token);
//		System.out.println("Current Identity : "+ScopeUtils.getCurrentCaller());
//		
//		SecurityTokenProvider.instance.set(toRestore);
//		System.out.println("Current Identity : "+ScopeUtils.getCurrentCaller());
//		
//		
		System.out.println(ISUtils.queryForDB("postgresql", "internal-db"));
		System.out.println("Checking for postgis .. ");
		System.out.println(ISUtils.queryForDB("postgis", "Concessioni"));
		
		
		WorkspaceManager wsManager=new WorkspaceManager(null);
		FolderContainer folder=wsManager.getApplicationBaseFolder();
		System.out.println("Base folder path : "+folder.get().getPath());
		System.out.println("Content : ");
		for(Item item:wsManager.getApplicationBaseFolder().list().getItems()) {
			System.out.println(item.getName());
		}
		
		SDIManager sdiManager=new SDIManager();
		RemoteFileDescriptor remoteGSFolder=sdiManager.getGeoServerRemoteFolder();
		System.out.println("GS Folder is "+remoteGSFolder);
		
		
		
		
//		System.out.println("Inspecting internal DB ");
//		System.out.println("Found "+ManagerFactory.getList().size()+" records");
		
		
		
		// INIT APP
		// INTERNAL DB 	// ******* IS ******** SE platform : "postgresql" AP."GNA_DB"="internal-db"
		// WORKSPACE ROOT FOLDER
		
		
		// INIT LAYERS
		
			//CENTROIDS
			// IS 		// ******* IS ******** SE platform : "postgis" AP."GNA_DB"="Concessioni"
			// DB TABLES : CONCESSIONI
			// WS, STORE, LAYER, STYLES
		
			//CONCESSIONI
			// WS, STORE, STYLES
			// ???? INDEXES
			
		
	}

}
