package org.gcube.application.geoportal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.gcube.application.geoportal.model.db.DatabaseConnection;
import org.gcube.application.geoportal.model.db.PostgisTable;
import org.gcube.application.geoportal.model.db.PostgisTable.Field;
import org.gcube.application.geoportal.model.db.PostgisTable.POINT;
import org.gcube.application.geoportal.model.fault.DataParsingException;
import org.gcube.application.geoportal.model.gis.BBOX;
import org.gcube.application.geoportal.storage.PostgisDBManagerI;

public class PostgisMockup implements PostgisDBManagerI {

	
	@Override
	public void commit() throws SQLException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public int deleteByFieldValue(PostgisTable target, Field field, Object value) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public DatabaseConnection getConnectionDescriptor() {
		return null;
	}

	@Override
	public void deleteTable(String tableName) throws SQLException {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public BBOX evaluateBoundingBox(PostgisTable table) throws SQLException, DataParsingException {
		return BBOX.WORLD_EXTENT;
	}
	
	
	@Override
	public PreparedStatement prepareInsertStatement(PostgisTable target, boolean createTable, boolean geometryAsText)
			throws SQLException {
		return new FakePreparedStatement();
	}
	@Override
	public void truncate(String tableName) throws SQLException {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public void create(PostgisTable toCreate) throws SQLException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public POINT evaluateCentroid(PostgisTable table) throws SQLException, DataParsingException {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ResultSet queryAll(PostgisTable table) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
