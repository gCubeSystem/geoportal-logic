package org.gcube.application.geoportal;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.gcube.application.geoportal.managers.DefatulEMFProvider;
import org.gcube.application.geoportal.managers.EMFProvider;

public class HibernateTest {

	public static void main(String[] args) {
		TokenSetter.set("/gcube/devsec/devVRE");
		EMFProvider provider=new DefatulEMFProvider();
		EntityManager manager=provider.getFactory().createEntityManager();
		EntityTransaction t=manager.getTransaction();
		t.begin();
//		manager.createNativeQuery("Update uploadedimages set record_id="" where id = "").executeUpdate();
		t.commit();
		manager.close();
		provider.shutdown();
	}
	
}
