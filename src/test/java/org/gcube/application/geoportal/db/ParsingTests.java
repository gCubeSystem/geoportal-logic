package org.gcube.application.geoportal.db;

import org.gcube.application.geoportal.model.fault.DataParsingException;
import org.gcube.application.geoportal.utils.DBUtils;
import org.junit.Test;

public class ParsingTests {

	@Test
	public void parseExtent() throws DataParsingException {
		String toparse="BBOX POLYGON((8.476 39.179,8.476 45.772,17.391 45.772,17.391 39.179,8.476 39.179))";
		System.out.println(DBUtils.parseST_Extent(toparse));
	}
}
