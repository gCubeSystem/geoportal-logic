package org.gcube.application.geoportal.db;

import java.io.File;
import java.io.FileFilter;

import org.gcube.application.geoportal.PostgisTableFactory;
import org.gcube.application.geoportal.model.db.PostgisTable.GeometryType;
import org.gcube.application.geoportal.utils.Files;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class PostgisTableTests {

	private static File csvFolder=null;

	@BeforeClass
	public static void init() {
		try {
			csvFolder=new File(System.getProperty("CSV_PATH"));
		}catch(RuntimeException e) {
			System.out.println("USING DEFAULT CSVs..");
			csvFolder=Files.getFileFromResources("csv");
		}
	}


//	@Test
//	public void testParseCSV() {
//		for(File csvFile:csvFolder.listFiles(new FileFilter() {
//
//			@Override
//			public boolean accept(File pathname) {
//				if(pathname.isDirectory()) return false;
//				return pathname.getName().endsWith(".csv");
//			}
//		})){
//			PostgisTableFactory f=new PostgisTableFactory(null);
//
//			try {
//				System.out.println(f.fromCSV(csvFile.getAbsolutePath()));
//			}catch(Throwable t) {
//				if(!csvFile.getName().contains("wrong")) {
//					t.printStackTrace();
//					Assert.fail();
//				}
//			}
//
//		}
//	}


}
