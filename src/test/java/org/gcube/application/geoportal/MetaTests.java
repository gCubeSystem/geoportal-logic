package org.gcube.application.geoportal;

import java.io.File;
import java.io.IOException;

import org.gcube.application.geoportal.utils.Files;
import org.gcube.spatial.data.geonetwork.iso.tpl.ISOMetadataByTemplate;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import freemarker.core.ParseException;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

public class MetaTests {

	@BeforeClass
	public static void init() throws IOException {
		File xmlDirectory=Files.getFileFromResources("iso_templates");
		Assert.assertTrue(xmlDirectory.exists());
		Assert.assertTrue(xmlDirectory.canRead());
		Assert.assertTrue(xmlDirectory.isDirectory());
		ISOMetadataByTemplate.registerTemplateFolder(xmlDirectory);
	}
	
//	@Test
//	public void generateMeta() throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException {
//		ISOMetadataByTemplate.custom(null, "mosi.ftlx");
//	}
}
