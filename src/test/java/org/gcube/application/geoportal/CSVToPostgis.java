package org.gcube.application.geoportal;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;

import org.gcube.application.geoportal.model.db.DBInteractionException;
import org.gcube.application.geoportal.model.fault.DataParsingException;

public class CSVToPostgis {

	
	public static void main(String[] args) throws FileNotFoundException, DBInteractionException, IOException, SQLException, DataParsingException {
		
		PostgisTableFactory factory=new PostgisTableFactory(new PostgisMockup());
		System.out.println(factory.fromCSV("/Users/FabioISTI/Downloads/progetti_encoded.csv"));
		
	}

}
