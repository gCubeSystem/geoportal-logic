package org.gcube.application.geoportal;

import java.io.IOException;

import org.gcube.application.geoportal.model.Record;
import org.gcube.application.geoportal.model.concessioni.Concessione;
import org.gcube.application.geoportal.model.content.WorkspaceContent;
import org.gcube.application.geoportal.model.report.PublicationReport;
import org.gcube.application.geoportal.utils.Serialization;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class SerializationTests {

	private Concessione concessione=null;

	@Before
	public void init() {
		concessione=TestModel.prepareConcessione();
		WorkspaceContent content=new WorkspaceContent();
		content.setId(1234);
		content.setLink("aflkjaflkj");
		content.setMimetype("mimetype");
		content.setStorageID("storage id");
		concessione.getRelazioneScavo().getActualContent().add(content);
	}

	@Test
	public void prettyPrint() throws JsonProcessingException {
		PublicationReport rep=new PublicationReport("my report");
				rep.setTheRecord(concessione);
		System.out.println(rep.prettyPrint());
	}

	@Test
	public void serilalization() throws JsonParseException, JsonMappingException, IOException {
		System.out.println(cycle(concessione));
	}

	private String cycle(Record record) throws JsonParseException, JsonMappingException, IOException {
		String s=Serialization.asJSON(record);
		Object read=Serialization.readObject(s, record.getClass());
		Assert.assertEquals(record,read);
		return s;
	}
}
