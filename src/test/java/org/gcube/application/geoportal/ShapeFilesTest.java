package org.gcube.application.geoportal;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Collections;

import org.gcube.application.geoportal.utils.Files;
import org.geotoolkit.data.DataStore;
import org.geotoolkit.data.DataStoreFactory;
import org.geotoolkit.data.DataStoreFinder;
import org.geotoolkit.storage.DataStoreException;

public class ShapeFilesTest {

	public static void main(String[] args) throws MalformedURLException, DataStoreException {
		
		File shp=Files.getFileFromResources("concessioni/pos.shp");
		System.out.println("File "+shp.getAbsolutePath()+" : "+shp.getTotalSpace()+"b");
//		DataStoreFinder.scanForPlugins();
		System.out.println("Factories are");
//		DataStoreFinder.getAllFactories(null).forEach((DataStoreFactory f)->{
//			System.out.println(f.getDisplayName()+"\t"+f.getDescription());
//		});
		System.out.println("Opening : "+shp.toURI().toURL());
		DataStore inputDataStore = DataStoreFinder.open(
		        Collections.singletonMap("url", shp.toURI().toURL()));
		
		System.out.println("Datastore is "+inputDataStore);
		
		
		System.out.println(inputDataStore.getTypeNames());
		
		
//		String filename="some Une+* / \\ []() l:];kaj.sd\"sfl.blah";
//		System.out.println(Files.fixFilename(filename));
		
	}

}
