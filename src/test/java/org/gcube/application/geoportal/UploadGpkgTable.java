package org.gcube.application.geoportal;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Map;

import org.gcube.application.geoportal.model.db.DBInteractionException;
import org.gcube.application.geoportal.model.db.DatabaseConnection;
import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.application.geoportal.model.fault.DataParsingException;
import org.gcube.application.geoportal.model.fault.GeoPackageInteractionException;
import org.gcube.application.geoportal.storage.PostgisDBManager;
import org.gcube.application.geoportal.utils.GpkgUtils;
import org.gcube.application.geoportal.utils.MainUtils;

public class UploadGpkgTable {

	private static final String USAGE="<OPTIONS>"
//			+ "CMD : upload, cleanup, list"
			+ "options : "
//			+ "--UPLOAD_TYPE : CONCESSIONI | TEMPLATE | GPKG_TABLE"
			+ "--DBUSER"
			+ "--DBPWD"
			+ "--DBURL"
//			+ "--CSVFile"
//			+ "--CONTEXT"
//			+ "--SHALLOW"
//			+ "--REMOVE_EXISTENT"
			+ "--GPKG"
			+ "--TABLENAME";
	
	
	public static void main(String[] args) throws SQLException, IOException, GeoPackageInteractionException, DBInteractionException, DataParsingException, ConfigurationException {
	
		if(args.length<1) {
			System.out.println(USAGE);
			System.exit(0);
		}
		
		String cmd=args[0];

		Map<String,String> options=MainUtils.asMap(args);
		System.out.println("CMD : "+cmd);
		System.out.println("Options : "+options);
		
		
		DatabaseConnection postgisConnection= new DatabaseConnection(MainUtils.getMandatory("DBUSER",options),
				MainUtils.getMandatory("DBPWD",options), MainUtils.getMandatory("DBURL",options));
		
		postgisConnection.setAutocommit(false);
		
		PostgisDBManager.init(postgisConnection);
		
		File gpkgFile=new File(MainUtils.getMandatory("GPKG",options));
		
		String tablename=MainUtils.getMandatory("TABLENAME",options);
		
		System.out.println(new PostgisTableFactory(PostgisDBManager.get()).fromGPKGFeatureTable(GpkgUtils.open(gpkgFile), tablename));
		
	}

}
