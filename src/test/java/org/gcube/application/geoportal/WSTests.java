package org.gcube.application.geoportal;

import java.util.List;

import org.gcube.application.geoportal.utils.Workspace;
import org.gcube.common.storagehub.client.dsl.ContainerType;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.ItemContainer;
import org.gcube.common.storagehub.client.dsl.ListResolver;
import org.gcube.common.storagehub.client.dsl.ListResolverTyped;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.common.storagehub.model.items.Item;

public class WSTests {

	public static void main(String[] args) throws StorageHubException {
		TokenSetter.set("/gcube/devsec/devVRE");
		
		StorageHubClient client=Workspace.getClient();
		String id="f835cd8a-0a92-4266-8c33-0b05e202ed94";
		
//		FolderContainer item=client.open(id).asFolder();
		
		FolderContainer item=client.openVREFolder().openByRelativePath(".GNA_RECORDS").asFolder();
		System.out.println(item.get().getPath());
//		switch(item.getType()) {
//		case FILE : 
//			FileContainer file=(FileContainer) item;
//			file.getAnchestors();
//		case FOLDER : 
//		}
//		System.out.println(path(item));
//		
//		System.out.println("TREE : ");
		
		boolean showHidden=true;
		printTree("", item,showHidden);
		
	}
	
	
	
	public static String path(ItemContainer item) throws StorageHubException {
		StringBuilder builder=new StringBuilder();
		ListResolver resolver=item.getAnchestors();
		for(Item i:resolver.getItems()) {
			builder.append(i.getName()+" ID : "+i.getId()+"\n");
		}
		return builder.toString();
	}

	
	
	public static void printTree(String padding,ItemContainer item,boolean showHidden){
		System.out.println(padding + "" +item.get().getName() +" [ID : "+item.getId()+", hidden = "+item.get().isHidden()+"]" );
		if(item.getType().equals(ContainerType.FOLDER)) {
			FolderContainer c =((FolderContainer)item);
			String newPadding=padding+"\t";
			List<? extends ItemContainer> l;
			try {
				ListResolverTyped lt=c.list();
				if(showHidden)lt.includeHidden();
				l = lt.getContainers();
			} catch (StorageHubException e) {
				throw new RuntimeException("Errore",e);			
			}
			l.forEach((ItemContainer i)->{printTree(newPadding, i,showHidden);});
		}
	}
}
