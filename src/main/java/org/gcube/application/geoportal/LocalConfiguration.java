package org.gcube.application.geoportal;

import java.net.URL;
import java.util.Properties;


import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
@Slf4j
public class LocalConfiguration {

	
	static LocalConfiguration instance=null;
	
	
	@Synchronized
	public static LocalConfiguration init(URL propertiesURL){
		if(instance==null)
			instance=new LocalConfiguration(propertiesURL);
		return instance; 
	}
	
	private Properties props=new Properties();
	
	private LocalConfiguration(URL propertiesURL) {
		try{
			log.debug("Loading {} ",propertiesURL);
			props.load(propertiesURL.openStream());
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}
	
	public static String getProperty(String property){
		return instance.props.getProperty(property);
	}
	
	public static String getProperty(String property,String defaultValue){
		return instance.props.getProperty(property, defaultValue);
	}
	
	public static Long getTTL(String property) {
		return Long.parseLong(getProperty(property));
	}
	
	public static boolean getFlag(String property) {
		return Boolean.parseBoolean(property);
	}
	
//	private static Object templateConfiguration=null;
//	
//	public static Object getTemplateConfigurationObject() {return templateConfiguration;}
//	public static void setTemplateConfigurationObject(Object obj) {templateConfiguration=obj;}
	
}
	
