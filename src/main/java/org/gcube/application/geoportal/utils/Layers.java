package org.gcube.application.geoportal.utils;

import org.gcube.spatial.data.geonetwork.LoginLevel;
import org.gcube.spatial.data.gis.model.report.PublishResponse;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public class Layers {


//	static {
//		// Register custom metadata template
//		try {
//			ISOMetadataByTemplate.registerTemplateFolder(Files.getFileFromResources("iso_templates/macros.ftlx"));
//		}catch(Throwable t) {
//			log.error("UNABLE TO REGISTER TEMPLATES",t);
//		}
//	}
//	
//	public static void deleteLayer(LayerDescriptor desc, GISInterface gis, GeoServerDescriptor geoserver,LoginLevel loginLevel, PostgisDBManagerI db) throws Exception {
//		// delete fte
//		gis.deleteLayer(desc.getGeoserverWorkspace(), desc.getLayerName(), desc.getMetaID(), geoserver, loginLevel);
//		// delete postgis table
//		db.deleteTable(desc.getTableName());
//
//		db.commit();
//		//TODO
//		// delete from WS
//	}
//
//
//
//	public static LayerDescriptor publishPostgisTable(PostgisTable table,GISInterface gis, GeoNetworkPublisher geonetwork, String layerTitle, String workspace, String storeName) throws Exception {
//
//
//
//		String DEFAULT_CRS="GEOGCS[\"WGS 84\", \n" + 
//				"  DATUM[\"World Geodetic System 1984\", \n" + 
//				"    SPHEROID[\"WGS 84\", 6378137.0, 298.257223563, AUTHORITY[\"EPSG\",\"7030\"]], \n" + 
//				"    AUTHORITY[\"EPSG\",\"6326\"]], \n" + 
//				"  PRIMEM[\"Greenwich\", 0.0, AUTHORITY[\"EPSG\",\"8901\"]], \n" + 
//				"  UNIT[\"degree\", 0.017453292519943295], \n" + 
//				"  AXIS[\"Geodetic longitude\", EAST], \n" + 
//				"  AXIS[\"Geodetic latitude\", NORTH], \n" + 
//				"  AUTHORITY[\"EPSG\",\"4326\"]]";
//
//
//		GSFeatureTypeEncoder fte=new GSFeatureTypeEncoder();
//		fte.setEnabled(true);
//		//		fte.setLatLonBoundingBox(-180.0, -90.0, 180.0, 90.0, DEFAULT_CRS);
//		BBOX bbox=table.getBoundingBox();
//		fte.setLatLonBoundingBox(bbox.getMinLong(), bbox.getMinLat(), bbox.getMaxLong(), bbox.getMaxLat(), DEFAULT_CRS);
//		fte.setName(table.getTablename());
//		fte.setTitle(layerTitle);
//		//		fte.setNativeCRS(DEFAULT_CRS);
//
//		GSLayerEncoder le=new GSLayerEncoder();
//
//		String defaultStyle=null;
//
//		switch(table.getGeometryColumnType()) {
//		case LINE : {
//			defaultStyle = "line";
//			break;
//		}case POINT: {
//			defaultStyle="point";
//			break;
//		}case POLYGON: {
//			defaultStyle = "polygon";
//			break;
//		}
//
//		}
//
//		le.setDefaultStyle(defaultStyle);
//		le.setEnabled(true);
//
//		Metadata geonetworkMeta=null;//generateMeta(null);
//
//		//publishDBTable
//		//		GISInterface gis=GISInterface.get(geoserver);
//
//		PublishResponse resp = gis.publishDBTable(workspace, 
//				storeName, 
//				fte, 
//				le, 
//				geonetworkMeta, 
//				geonetwork.getCurrentUserConfiguration("dataset", "_none_"),
//				LoginLevel.DEFAULT, 
//				false);
//
//		log.debug("REsult : "+resp);
//
//		if(resp.getDataOperationResult()!=OperationState.COMPLETE||resp.getMetaOperationResult()!=OperationState.COMPLETE) {
//			throw new PublishException("Outcome was not COMPLETE ",resp);
//		}
//		String metadataUUID=geonetwork.getInfo(resp.getReturnedMetaId()).getUuid();
//
//		return new LayerDescriptor(
//				metadataUUID,
//				resp.getReturnedMetaId(),
//				fte.getName(),
//				workspace,
//				table.getTablename());
//	}


//	private static Metadata generateMetaForTemplate(MOSI mosi, LayerDescriptor layerDescriptor,BBOX bbox)  {
//		try {
//			// TODO Set layer info
//			
//			mosi.setExtent(new Extent_Type(bbox.getMinLong(), bbox.getMaxLong(), bbox.getMaxLat(), bbox.getMinLat()));
//
//			
//			
//			File metaFile=;
//			return (Metadata) XML.unmarshal(metaFile);
//		}catch(IOException | TemplateException | JAXBException e) {
//			throw new RuntimeException("WORK ON THIS");
//		}
//	}

	
	public static void publishShapeFile() {
		String DEFAULT_CRS="GEOGCS[\"WGS 84\", \n" + 
				"  DATUM[\"World Geodetic System 1984\", \n" + 
				"    SPHEROID[\"WGS 84\", 6378137.0, 298.257223563, AUTHORITY[\"EPSG\",\"7030\"]], \n" + 
				"    AUTHORITY[\"EPSG\",\"6326\"]], \n" + 
				"  PRIMEM[\"Greenwich\", 0.0, AUTHORITY[\"EPSG\",\"8901\"]], \n" + 
				"  UNIT[\"degree\", 0.017453292519943295], \n" + 
				"  AXIS[\"Geodetic longitude\", EAST], \n" + 
				"  AXIS[\"Geodetic latitude\", NORTH], \n" + 
				"  AUTHORITY[\"EPSG\",\"4326\"]]";
		
	}
	
	
	
	
}
