package org.gcube.application.geoportal.utils;

import java.util.HashMap;
import java.util.Map;

public class MainUtils {


	public static final String getMandatory(String key,Map<String,String> map) {
		if(map.containsKey(key)) return map.get(key);
		else throw new RuntimeException("Missing mandatory parameter "+key);
	}

	public static final String getOptional(String key,Map<String,String> map, String defaultValue) {
		if(map.containsKey(key)) return map.get(key);
		else return defaultValue;
	}

	public static Map<String,String> asMap(String[] args){
		HashMap<String,String> toReturn=new HashMap<>();
		for(String arg:args) {
			if(arg.contains("="))
				toReturn.put(arg.substring(2, arg.indexOf("=")), // skipping first 2 chars "--XX="  
						arg.substring(arg.indexOf("=")+1));			
		}
		return toReturn;
	}
}
