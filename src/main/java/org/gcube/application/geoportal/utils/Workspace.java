package org.gcube.application.geoportal.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.gcube.common.storagehub.client.dsl.FileContainer;
import org.gcube.common.storagehub.client.dsl.FolderContainer;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;

public class Workspace {

	public static final StorageHubClient getClient() {
		return new StorageHubClient();		 
	}
	
//	public static FolderContainer getFolderByPath(String path,StorageHubClient client) {
//		String toLookFor=path;
//		while(toLookFor.contains(File.separator)) {
//			String currentLevel=toLookFor.substring(0, endIndex)
//		}
//		
//		client.getWSRoot().get;
//		
//	}
	
	public static final FolderContainer getFolderById(String id, StorageHubClient client) throws StorageHubException {
		return client.open(id).asFolder();
	}
	
	public static final FileContainer storeByFolderId(FolderContainer destination, File toStore, StorageHubClient client) throws StorageHubException, IOException {
		FileContainer toReturn=null;
		try(InputStream is = new FileInputStream(toStore)){
			toReturn = destination.uploadFile(is, "name", "description");
		   } catch (StorageHubException | IOException e) {
			throw e;
		   }
		return toReturn;
	}
	
}
