package org.gcube.application.geoportal.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.gcube.application.geoportal.model.fault.DataParsingException;
import org.gcube.application.geoportal.model.gis.BBOX;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DBUtils {

	
	private static Pattern pattern = Pattern.compile("(?!=\\d\\.\\d\\.)([\\d.]+)");

	public static BBOX parseST_Extent(String extent) throws DataParsingException {
		//BOX(11.9122574810083 44.2514144864263,11.9761128271586 44.2912342569845)
		try {
			log.debug("Parsing BBOX "+extent);
			Matcher m=pattern.matcher(extent);

			//				Scanner sc = new Scanner(extent);
			//				double minLong = sc.nextDouble(), 
			//				       minLat = sc.nextDouble(), 
			//				       maxLong = sc.nextDouble(),
			//				       maxLat= sc.nextDouble();

			if(!m.find()) throw new DataParsingException("Unable to get minLong ");
			Double minLong=Double.parseDouble(m.group(1));

			if(!m.find()) throw new DataParsingException("Unable to get minLat ");				
			Double minLat=Double.parseDouble(m.group(1));

			if(!m.find()) throw new DataParsingException("Unable to get maxLong ");
			Double maxLong=Double.parseDouble(m.group(1));

			if(!m.find()) throw new DataParsingException("Unable to get maxLat ");
			Double maxLat=Double.parseDouble(m.group(1));
			return new BBOX(maxLat, maxLong, minLat, minLong);
		}catch(Throwable t) {
			throw new DataParsingException("Invalid BBOX "+extent,t);
		}
	}
}
