package org.gcube.application.geoportal.utils;

import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.collections4.CollectionUtils;

public class CollectionsUtils {

	public static boolean equalsCollections(Collection<?> a, Collection<?> b) {
		
		if(a==null&&b!=null) 
			return false;
		if(a!=null&&b==null) 
			return false;
		
		if(a==null&&b==null) 
			return true;
		
		if(a.size()!=b.size()) 
			return false;
		
		Iterator<?> itA=a.iterator();
		Iterator<?> itB=a.iterator();
		while(itA.hasNext()) {
			if(!itA.next().equals(itB.next())) 
				return false;
		}
		
		return true;
	}

	public static int hashCode(Collection<?> a) {		
		if(a==null || a.isEmpty())
			return 0;
		
		final int prime = 31;
		int result = 1;		
		
		Iterator<?> it=a.iterator();
		while(it.hasNext())
			result=prime*result+it.next().hashCode();
		return result;		
	}
	
}
