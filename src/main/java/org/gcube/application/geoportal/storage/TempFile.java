package org.gcube.application.geoportal.storage;

import java.io.File;

import lombok.Data;
import lombok.NonNull;

@Data class TempFile{
	@NonNull
	File theFile;
	@NonNull
	String originalFileName;
}