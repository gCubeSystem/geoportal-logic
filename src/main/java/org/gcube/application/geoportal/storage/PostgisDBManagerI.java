package org.gcube.application.geoportal.storage;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.gcube.application.geoportal.model.db.DatabaseConnection;
import org.gcube.application.geoportal.model.db.PostgisTable;
import org.gcube.application.geoportal.model.db.PostgisTable.Field;
import org.gcube.application.geoportal.model.db.PostgisTable.POINT;
import org.gcube.application.geoportal.model.fault.DataParsingException;
import org.gcube.application.geoportal.model.gis.BBOX;

public interface PostgisDBManagerI {

	void commit() throws SQLException;

//	int insertCentroid(CentroidRecord record) throws SQLException;

//	int deleteCentroid(String uuid) throws SQLException;

	BBOX evaluateBoundingBox(PostgisTable table) throws SQLException, DataParsingException;

	PreparedStatement prepareInsertStatement(PostgisTable target, boolean createTable, boolean geometryAsText)
			throws SQLException;
	

	void deleteTable(String tableName) throws SQLException;

	void truncate(String tableName) throws SQLException;

	void create(PostgisTable toCreate) throws SQLException;

	POINT evaluateCentroid(PostgisTable table) throws SQLException, DataParsingException;

	
	ResultSet queryAll(PostgisTable table) throws SQLException;

	int deleteByFieldValue(PostgisTable target, Field field, Object value) throws SQLException;
	
	DatabaseConnection getConnectionDescriptor();
}