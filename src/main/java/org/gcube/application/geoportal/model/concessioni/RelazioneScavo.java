package org.gcube.application.geoportal.model.concessioni;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;

import org.gcube.application.geoportal.model.content.AssociatedContent;
import org.gcube.application.geoportal.model.report.ValidationReport;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper=true)
@Entity
@DiscriminatorValue("RELAZIONE")
public class RelazioneScavo extends AssociatedContent {

	
	@Type(type="text")
	private String abstractSection;
	
	@Type(type="text")
	private String abstractIta;
	@Type(type="text")
	private String abstractEng;
	
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass=String.class)
	private List<String> responsabili;
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass=String.class)
	private List<String> soggetto;

	
	@Override
	public ValidationReport validateForInsertion() {
		
		ValidationReport toReturn=super.validateForInsertion();
		toReturn.setObjectName("Relazione Scavo");
		toReturn.checkMandatory(responsabili, "Responsabili");
		toReturn.checkMandatory(soggetto, "Soggetto");
		return toReturn;
	}
	
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = super.hashCode();
//		result = prime * result + ((abstractSection == null) ? 0 : abstractSection.hashCode());
////		result = prime * result + ((responsabili == null) ? 0 : responsabili.hashCode());
//		result = prime * result + CollectionsUtils.hashCode(responsabili);
////		result = prime * result + ((soggetto == null) ? 0 : soggetto.hashCode());
//		result = prime * result + CollectionsUtils.hashCode(soggetto);
//		return result;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (!super.equals(obj))
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		RelazioneScavo other = (RelazioneScavo) obj;
//		if (abstractSection == null) {
//			if (other.abstractSection != null)
//				return false;
//		} else if (!abstractSection.equals(other.abstractSection))
//			return false;
////		if (responsabili == null) {
////			if (other.responsabili != null)
////				return false;
////		} else if (!responsabili.equals(other.responsabili))
////			return false;
//		if(!CollectionsUtils.equalsCollections(responsabili, other.responsabili)) return false;
//		
//		
////		if (soggetto == null) {
////			if (other.soggetto != null)
////				return false;
////		} else if (!soggetto.equals(other.soggetto))
////			return false;
//		if(!CollectionsUtils.equalsCollections(soggetto, other.soggetto)) return false;
//		
//		
//		return true;
//	}
//	
}
