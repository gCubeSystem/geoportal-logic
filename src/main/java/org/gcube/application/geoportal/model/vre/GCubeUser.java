package org.gcube.application.geoportal.model.vre;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class GCubeUser {

	private String id;
	private String name;
	private String currentVRE;
}
