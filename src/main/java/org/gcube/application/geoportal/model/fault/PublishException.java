package org.gcube.application.geoportal.model.fault;

import org.gcube.spatial.data.gis.model.report.PublishResponse;

import lombok.NonNull;

public class PublishException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1356876669436308224L;
	
	@NonNull
	private PublishResponse resp;

	public PublishException(PublishResponse resp) {
		super();
		this.resp=resp;
	}

	public PublishException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace,PublishResponse resp) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.resp=resp;
	}

	public PublishException(String message, Throwable cause,PublishResponse resp) {
		super(message, cause);
		this.resp=resp;
	}

	public PublishException(String message,PublishResponse resp) {
		super(message);
		this.resp=resp;
	}

	public PublishException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	
}
