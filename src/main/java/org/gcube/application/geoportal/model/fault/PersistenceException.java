package org.gcube.application.geoportal.model.fault;

import org.gcube.application.geoportal.model.report.PublicationReport;


public class PersistenceException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6607258789784961416L;
	PublicationReport report;

	public PersistenceException(PublicationReport report) {
		super();
		this.report = report;
	}

	public PersistenceException(String message, PublicationReport report) {
		super(message);
		this.report = report;
	}

	public PersistenceException(String message, Throwable cause, PublicationReport report) {
		super(message, cause);
		this.report = report;
	}

	public PersistenceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace,
			PublicationReport report) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.report = report;
	}
	
	public PublicationReport getReport() {
		return report;
	}
}
