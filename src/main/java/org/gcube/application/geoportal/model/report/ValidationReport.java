package org.gcube.application.geoportal.model.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.gcube.application.geoportal.utils.Serialization;

import com.fasterxml.jackson.core.JsonProcessingException;

import lombok.Data;

@Data
public class ValidationReport implements Serializable{


	public static enum ValidationStatus{
		PASSED, ERROR, WARNING
	}


	private String objectName;


	private List<String> errorMessages=new ArrayList<String>();
	private List<String> warningMessages=new ArrayList<String>();
	private List<ValidationReport> children=new ArrayList<ValidationReport>();


	public ValidationStatus getStatus() {
		if(!errorMessages.isEmpty()) return ValidationStatus.ERROR;

		boolean isWarning=(!warningMessages.isEmpty());
		for(ValidationReport obj : children) {
			ValidationStatus status=obj.getStatus();
			if(status.equals(ValidationStatus.ERROR)) return ValidationStatus.ERROR;
			if(status.equals(ValidationStatus.WARNING)) isWarning=true;
		}
		if(isWarning) return ValidationStatus.WARNING;
		return ValidationStatus.PASSED;			
	}

	public void addChild(ValidationReport obj) {
		children.add(obj);
	}

	public void addMessage(ValidationStatus status,String message) {
		switch (status) {
		case ERROR:
			errorMessages.add(message);
			break;
		case WARNING :
			warningMessages.add(message);
		default:
			break;
		}
	}

	
	
	
	public <T> boolean checkMandatory(T toCheck,String name,Check...checks) {
		ConstraintCheck<T> check=new ConstraintCheck<T>(toCheck,name,checks);
		if(check.isError())
			this.addMessage(ValidationStatus.ERROR, check.getMessage());
		return !check.isError();			
	}


	public ValidationReport(String objectName) {
		super();
		this.objectName = objectName;
	}

	public ValidationReport() {
	}

	public String prettyPrint() throws JsonProcessingException {
		return Serialization.prettyPrint(this);
	}


}
