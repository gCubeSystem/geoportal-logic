package org.gcube.application.geoportal.model.db;

import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.application.geoportal.utils.ISUtils;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@RequiredArgsConstructor
@ToString
public class DatabaseConnection {

	public static enum TYPE{
		
		Concessioni,Mosi,Mopr
	}
	
	public static DatabaseConnection get(TYPE type) throws ConfigurationException {
		return ISUtils.queryForDB("postgis", type.toString());
	}
	
	@NonNull
	private String user;
	@NonNull
	private String pwd;
	@NonNull
	private String url;
	
	private Boolean autocommit=false;
}
