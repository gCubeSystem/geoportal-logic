package org.gcube.application.geoportal.model;

public enum AccessPolicy {

	OPEN,RESTRICTED,EMBARGOED;
	
}
