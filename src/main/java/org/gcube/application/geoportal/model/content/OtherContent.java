package org.gcube.application.geoportal.model.content;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper=true)

@Entity
@DiscriminatorValue("OTHER")
public class OtherContent extends AssociatedContent {

	

}
