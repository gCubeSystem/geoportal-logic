package org.gcube.application.geoportal.model.fault;

import org.gcube.application.geoportal.model.report.ValidationReport;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6614557911302878653L;
	private ValidationReport report;
	public ValidationException(ValidationReport report) {
		super();
		this.report = report;
	}
	
	public ValidationException(ValidationReport report, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		this.report = report;
	}
	public ValidationException(ValidationReport report, String message, Throwable cause) {
		super(message, cause);
		this.report = report;
	}
	public ValidationException(ValidationReport report, String message) {
		super(message);
		this.report = report;
	}
	public ValidationException(ValidationReport report, Throwable cause) {
		super(cause);
		this.report = report;
	}
	
	

}
