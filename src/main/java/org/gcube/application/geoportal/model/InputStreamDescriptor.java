package org.gcube.application.geoportal.model;

import java.io.InputStream;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Data
@Getter
@Setter
public class InputStreamDescriptor{

	@NonNull
	private InputStream stream;
	@NonNull
	private String filename;
	
	
}
