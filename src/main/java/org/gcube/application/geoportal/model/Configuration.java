package org.gcube.application.geoportal.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@Setter
@ToString
public class Configuration {

	@NonNull
	private Boolean useTestSuffixes;
	
	
	@NonNull
	private String workspacePreventiva;

	@NonNull
	private String workspaceConcessioni;
	
	@NonNull
	private String workspaceArcheomar;
	
	
	@NonNull
	private String postgisStore;
	
	
	@NonNull
	private Boolean cleanUpBeforeUpdate=true;
}
