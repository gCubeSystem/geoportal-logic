package org.gcube.application.geoportal.model.content;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@EqualsAndHashCode
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
    @Type(value = GeoServerContent.class),
    @Type(value = WorkspaceContent.class),
    })
public abstract class PersistedContent {

	//Generic Info
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;	

	@ManyToOne
	@JoinColumn(name="content_id", nullable=false)
	private AssociatedContent associated;

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PersistedContent [id=");
		builder.append(id);
		if(associated==null) {
			builder.append(", associated=");
			builder.append(associated);
		}else {
			builder.append(", OWNER-associated-id=");
			builder.append(associated.getId());
		}
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
