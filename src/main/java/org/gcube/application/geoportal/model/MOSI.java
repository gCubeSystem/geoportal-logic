package org.gcube.application.geoportal.model;

import java.util.Collection;
import java.util.HashMap;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@AllArgsConstructor
public class MOSI {

	public static final HashMap<String,String> FIELDS_DESCRIPTION=new HashMap<String,String>();
	
	static {
		//CD
		FIELDS_DESCRIPTION.put("TSK", "Tipo modulo");
		FIELDS_DESCRIPTION.put("CDM", "Tipo modulo");
		FIELDS_DESCRIPTION.put("ESC", "Tipo modulo");
		FIELDS_DESCRIPTION.put("ECP", "Tipo modulo");
		FIELDS_DESCRIPTION.put("CBC", "Tipo modulo");
		FIELDS_DESCRIPTION.put("TSK", "Tipo modulo");
		FIELDS_DESCRIPTION.put("TSK", "Tipo modulo");
		FIELDS_DESCRIPTION.put("TSK", "Tipo modulo");
		FIELDS_DESCRIPTION.put("TSK", "Tipo modulo");
	}
	
	// IDENTIFICAZIONE
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class CD_Type{
		
		@Data
		@AllArgsConstructor		
		public static class ACC_Type{
			//Ente/Soggetto Responsabile
			@NonNull
			private String ACCE;
			//Codice Identificativo
			@NonNull
			private String ACCC;
			//Note
			private String ACCS; 
		}
		
		
		// Tipo Modulo
		// CL VC_TSK_MOSI
		@NonNull
		private String TSK;
		
		//Codice Modulo
		@NonNull
		private String CDM;
		
		//Ente Schedatore				
		private String ESC;
		
		//Ente competente per tutela
		private String ECP;
		
		//Identificativo scheda bene culturale
		private String CBC;
		
		//ALTRO CODICE
		@NonNull
		private Collection<ACC_Type> ACC; 
		
	}
	
	// AREA/SITO
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class OG_Type{
		//Ambito di tutela MiBACT
		@NonNull
		private String AMB;
		
		//Ambito di applicazione
		@NonNull
		private String AMA;
		
		//Definizione
		@NonNull
		private String OGD;
		
		//Tipologia
		private String OGT;
		
		//Denominazione
		private Collection<String> OGN;
	}
	
	//LOCALIZZAZIONE
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class LC_Type{
		
		@Data
		@AllArgsConstructor
		public static class ACB_Type{
			//Accessibilità
			private String ACBA;
			//Note
			private String ACBS;
		}
		
		//Stato
		@NonNull		
		private String LCS;
		//Regione
		@NonNull		
		private String LCR;
		//Provincia
				@NonNull		
				private String LCP;
				//Comune
				@NonNull		
				private String LCC;
				
				//Indirizzo
				private String LCI;
				//Toponimo
				private Collection<String> PVL;
				
				//Tipo di contesto
				private String PVZ;
				
				//Accessibilità
				private ACB_Type ACB;
	}
	
	//CRONOLOGIA
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class DT_Type{
		//Riferimento Cronologico
		@NonNull
		private String DTR;
		//Note
		private String DTT;
		
	}
	
	//DATI ANALITICI
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class DA_Type{
		//Descrizione
		@NonNull
		private String DES;
		
		//Modalità di individuazione
		@NonNull
		private Collection<String> OGM;
	}
	
	//GEOREFERENZIAZIONE
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class GE_Type{
		
		//COORDINATE
		@Data
		@AllArgsConstructor
		public static class GEC_Type{
			@NonNull
			private String GECX;
			@NonNull
			private String GECY;
		}
		
		//BASE CARTOGRAFICA
		@Data
		@AllArgsConstructor
		public static class GPB_Type{
			//Descrizione Sintetica 
			@NonNull
			private String GPBB;
		}
		
		
		//Tipo di localizzazione 
		@NonNull
		private String GEL;
		
		//Tipo di georeferenziazione
		@NonNull
		private String GET;
		
		//Sistema di riferimento
		@NonNull
		private String GEP;
		
		//COORDINATE
		@NonNull
		private Collection<GEC_Type> GEC;
		
		//Tecnica di georeferenziazione
		@NonNull
		private String GPT;
		
		//Metodo di posizionamento
		@NonNull
		private String GPM;
		
		private GPB_Type GPB;
	}
	
	// CONDIZIONE GIURIDICA E PROVVEDIMENTI DI TUTELA
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class TU_Type{
		
		//CONDIZIONE GIURIDICA
		@Data
		@AllArgsConstructor
		public static class CDG_Type{
			//Indicazione generica
			@NonNull
			private String CDGG;
		}

		// PROVVEDIMENTI DI TUTELA
		@Data
		@AllArgsConstructor
		public static class NVC_Type{
			//Normativa di riferimento
			private String NVCT;
			//Provvedimento di tutela 
			private String NVCM;
			// Estremi di provvedimento
			private String NVCE;
			//Estensione del vincolo
			private String NVCP;
			//Note
			private String NVCN;
		}
		
		//STRUMENTI URBANISTICO - TERRITORIALI
		@Data
		@AllArgsConstructor
		public static class STU_Type{
			//Ente/Amministrazione
			private String STUE;
			//Tipo Strumento
			private String STUT;
			//Note
			private String STUS;
		}
		
		//CONDIZIONE GIURIDICA
		@NonNull
		private CDG_Type CDG;
		
		//Provvedimenti di tutela - sintesi
		@NonNull
		private String BPT;
		
		//PROVVEDIMENTI DI TUTELA 
		private Collection<NVC_Type> NVC;
		
		// STRUMENTI URBANISTICO - TERRITORIALI
		private Collection<STU_Type> STU;
		
		
	}
	
	// INDAGINI
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class RE_Type{
		//RICOGNIZIONE ARCHEOLOGICA
		@Data
		@AllArgsConstructor
		public static class RCG_Type{
			//Denominazione Ricognizione
			@NonNull
			private String RCGV;
			//Riferimento cronologico
			@NonNull
			private String RCGD;
			//Situazione ambientale
			private String RCGT;
			//Motivo
			// CL VC_Motivo_indagini_Mosi
			private String RCGE;
						
		}
		
		// PRESENZA MATERIALI
		@Data
		@AllArgsConstructor
		public static class MTP_Type{
			//Categoria materiale
			@NonNull
			private String MTPC;
			//Note
			private String MTPZ;			
		}
		
		//FOTOINTERPRETAIONE / FOTORESTITUZIONE
		@Data
		@AllArgsConstructor
		public static class FOI_Type{
			//Tipo immagine
			private String FOIT;
			//Riferimento cronologico
			private String FOIR;
			//Origine Anomalia
			private String FOIA;
			//Tipo anomalia
			private String FOIQ;
			//Classificazione anomalia
			private String FOIF;
			//Affidabilità
			private String FOIO;
			//Note
			private String FOIN;
		}
		
		
		@NonNull
		private RCG_Type RCG;
		
		@NonNull
		private Collection<MTP_Type> MTP; 

		//Assenza di materiali
		@NonNull
		private String MTZ;
		
		private Collection<FOI_Type> FOI;
		
	}
	
	// INDAGINI PREGRESSE
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public static class IP_Type{
		//INDAGINI ARCHEOLOGICHE PREGRESSE 
		@Data
		@AllArgsConstructor
		public static class IAP_Type{
			//Denominazione indagine 
			@NonNull
			private String IAPN;
			
			//Riferimento cronologico
			@NonNull
			private String IAPR;
			
			//Informazioni sull'indagine
			private String IAPI;
			
			//Note
			private String IAPS;
		}
		
		private Collection<IAP_Type> IAP;
	}

	
	// DATI TECNICI
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class MT_Type{
		//MISURE
		@Data
		@AllArgsConstructor
		public static class MIS_Type{
			//Tipo Misura
			private String MISZ;
			//Unità di misura
			private String MISU;
			// Valore
			private String MISM;
		}
		
		//ALTIMETRIA / QUOTE
		@Data
		@AllArgsConstructor
		public static class MTA_Type{
			//Riferimento
			private String MTAP;
			//Quota miniuma s.l.m
			private String MTAM;
			//Quota massima s.l.m
			private String MTAX;
			//Quota relativa
			private String MTAR;
			//Note
			private String MTAS;
		
		}
		
		
		private MIS_Type MIS;
		private Collection<MTA_Type> MTA;
	}
	
	
	//Valutazione / INTERPRETAZIONE
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class VR_Type{
		//VALUTAZIONE POTENZIALE ARCHEOLOGICO 
		@Data
		@AllArgsConstructor
		public static class VRP_Type{
			//Interpretazione
			@NonNull
			private String VRPI;
			//Affidabilità
			@NonNull
			private String VRPA;
			//Valutazione nell'ambito del contesto
			private String VRPV;
			//Potenziale - sintesi
			@NonNull
			private String VRPS;
			//Note
			private String VRPN;
		}
		
		
		//VALUTAZIONE RISCHIO ARCHEOLOGICO
		@Data
		@AllArgsConstructor
		public static class VRR_Type{
			//Codice progetto di riferimento
			@NonNull
			private String VRRP;
			//Distanza dall'opera in progetto
			@NonNull
			private String VRRO;
			//Valutazione rispetto all'opera in progetto
			@NonNull
			private String VRRR;
			//Rischio - sintesi
			@NonNull
			private String VRRS;
			//Note
			private String VRRN;
		}
		
		@NonNull
		private VRP_Type VRP;
		@NonNull
		private VRR_Type VRR;
	}
	
	// DOCUMENTAZIONE
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class DO_Type{
		//DOCUMENTAZIONE FOTOGRAFICA
		@Data
		@AllArgsConstructor
		public static class FTA_Type{
			//Codice identificativo
			private String FTAN;
			//Genere
			@NonNull
			private String FTAX;
			//Tipo
			@NonNull
			private String FTAP;
			//Collocazione 
			private String FTAC;
			//Nome file digitale
			private String FTAK;
			
		}
		//DOCUMENTAZIONE GRAFICA E CARTOGRAFICA
		@Data
		@AllArgsConstructor
		public static class DRA_Type{
			//Codice identificativo
			private String DRAN;
			//Genere
			@NonNull
			private String DRAX;
			//Tipo
			@NonNull
			private String DRAT;
			//Collocazione
			private String DRAC;
			//Nome File digitale
			private String DRAK;
		}
		
		//FONTI E DOCUMENTI
		@Data
		@AllArgsConstructor
		public static class FNT_Type{
			//Codice identificativo
			private String FNTI;
			//Genere
			@NonNull
			private String FNTX;
			//Tipo
			@NonNull
			private String FNTP;
			//Collocazione
			private String FNTS;
			//Come file digitale
			private String FNTK;
		}
		
		//BIBLIOGRAFIA
		@Data
		@AllArgsConstructor
		public static class BIB_Type{
			//Abbreviazione
			private String BIBR;
			//Genere
			@NonNull
			private String BIBX;
			//Riferimento bibliografico completo
			@NonNull
			private String BIBM;
			//Note
			private String BIBN;
		}
		
		@NonNull
		private Collection<FTA_Type> FTA;
		private Collection<DRA_Type> DRA;
		private Collection<FNT_Type> FNT;
		private Collection<BIB_Type> BIB;
		
	}
	
	// CERTIFICAZIONE E GESTIONE DATI
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class CM_Type{
		//Funzionario responsabile
		@NonNull
		private Collection<String> FUR;
		//Responsabile contenuti
		@NonNull
		private Collection<String> CMR;
		//Responsabile redazione modulo
		@NonNull
		private Collection <String> CMC;
		//Anno di redazione
		@NonNull
		private String CMA;
		//Profilo di accesso
		@NonNull
		private String ADP;
	}
	
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class Extent_Type{
		@NonNull
		private Double west;
		@NonNull
		private Double est;
		@NonNull
		private Double north;
		@NonNull
		private Double south;
	}

	@NonNull
	private CD_Type CD;
	
	@NonNull
	private OG_Type OG;
	
	@NonNull
	private LC_Type LC;
	
	@NonNull
	private DT_Type DT;
	@NonNull
	private DA_Type DA;
	
	@NonNull
	private GE_Type GE;
	
	@NonNull
	private TU_Type TU;
	
	@NonNull
	private RE_Type RE;
	
	private IP_Type IP;
	
	private MT_Type MT;
	
	@NonNull
	private VR_Type VR;
	@NonNull
	private DO_Type DO;
	
	@NonNull 
	private CM_Type CM;
	
	@NonNull
	// TO BE COMPUTED
	private Extent_Type extent;
	
	
	
	
	
}
