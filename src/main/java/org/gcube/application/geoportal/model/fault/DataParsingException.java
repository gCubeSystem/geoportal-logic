package org.gcube.application.geoportal.model.fault;

public class DataParsingException extends Exception {

	public DataParsingException() {
		// TODO Auto-generated constructor stub
	}

	public DataParsingException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DataParsingException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public DataParsingException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DataParsingException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
