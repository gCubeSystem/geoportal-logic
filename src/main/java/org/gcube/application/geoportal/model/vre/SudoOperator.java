package org.gcube.application.geoportal.model.vre;

import org.gcube.application.geoportal.model.fault.ConfigurationException;
import org.gcube.application.geoportal.utils.ISUtils;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.data.transfer.library.utils.ScopeUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SudoOperator {

	private String token;
	private String backupToken;
	private String caller;
	
	public SudoOperator() throws ConfigurationException {
		this.token=ISUtils.getToken();
		this.backupToken=SecurityTokenProvider.instance.get();
		this.caller=ScopeUtils.getCurrentCaller();
		log.warn("Created SUDO operator by "+caller);
	}
	
	public void sudo() {
		SecurityTokenProvider.instance.set(token);
		log.warn("Starting SUDO operation by "+ScopeUtils.getCurrentCaller());
	}
	
	public void reset() {
		SecurityTokenProvider.instance.set(backupToken);
	}
}
