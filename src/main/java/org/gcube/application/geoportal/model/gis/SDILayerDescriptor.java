package org.gcube.application.geoportal.model.gis;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import org.gcube.application.geoportal.model.content.AssociatedContent;



@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class SDILayerDescriptor extends AssociatedContent{

	
	
	public abstract Long getLayerID();
	public abstract void setLayerID(Long layerID);
	public abstract String getLayerUUID();
	public abstract void setLayerUUID(String layerUUID);
	public abstract String getLayerName();
	public abstract void setLayerName(String layerName);
	public abstract String getWmsLink();
	public abstract void setWmsLink(String wmsLink);
	public abstract void setWorkspace(String workspace);
	public abstract String getWorkspace();
	public abstract BBOX getBbox();
	public abstract void setBbox(BBOX toSet);
}
	