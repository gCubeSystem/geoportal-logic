package org.gcube.application.geoportal.model.content;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper=true)

@Entity
@DiscriminatorValue("WORKSPACE")
@EqualsAndHashCode(callSuper=true)
public class WorkspaceContent extends PersistedContent{

	private String mimetype;
	private String storageID;
	private String link;
}
