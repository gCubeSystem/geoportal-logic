package org.gcube.application.geoportal.model.content;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(callSuper=true)

@Entity
@DiscriminatorValue("GEOSERVER")
@EqualsAndHashCode(callSuper=true)
public class GeoServerContent extends PersistedContent{

		//GeoServer Details
		private String geoserverHostName;		
		private String geoserverPath;	
		
		@LazyCollection(LazyCollectionOption.FALSE)
		@ElementCollection(targetClass=String.class)
		private List<String> fileNames=new ArrayList<String>();
		
	
		private String workspace;
		private String store;
		private String featureType;
}
