package org.gcube.application.geoportal.model.fault;

public class GeoPackageInteractionException extends Exception {

	public GeoPackageInteractionException() {
		// TODO Auto-generated constructor stub
	}

	public GeoPackageInteractionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public GeoPackageInteractionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public GeoPackageInteractionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public GeoPackageInteractionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
