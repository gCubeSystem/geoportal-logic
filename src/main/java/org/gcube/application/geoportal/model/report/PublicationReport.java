package org.gcube.application.geoportal.model.report;

import java.io.Serializable;

import org.gcube.application.geoportal.model.Record;
import org.gcube.application.geoportal.utils.Serialization;

import com.fasterxml.jackson.core.JsonProcessingException;

import lombok.Getter;
import lombok.Setter;


public class PublicationReport extends ValidationReport implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1422004928222440165L;
	
	@Getter
	@Setter
	private Record theRecord;

	public PublicationReport(String objectName) {
		super(objectName);
	}
	
	public PublicationReport() {
	}
	
	@Override
	public String prettyPrint() throws JsonProcessingException {
		Record app=theRecord;
		theRecord=null;
		String toReturn=Serialization.prettyPrint(this);
		theRecord=app;
		return toReturn;
	}
}
