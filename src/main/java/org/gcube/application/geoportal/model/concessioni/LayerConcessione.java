package org.gcube.application.geoportal.model.concessioni;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;

import org.gcube.application.geoportal.model.gis.BBOX;
import org.gcube.application.geoportal.model.gis.SDILayerDescriptor;
import org.gcube.application.geoportal.model.report.ValidationReport;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString(callSuper=true)
@Entity
@DiscriminatorValue("LAYER_CONCESSIONE")
public class LayerConcessione extends SDILayerDescriptor{

	
	//meta
	private String layerUUID;
	private Long layerID;
	
	//layer
	private String layerName;
	private String workspace;
	
	@Type(type="text")
	private String wmsLink;
	
	
	

	
	//1.Identificazione
	@Type(type="text")
	private String abstractSection;
	
	//2.Classificazione 
	private String topicCategory;

	//3.Keyword
	private String subTopic;
	
	//4. Delimitazione geographica 
	private BBOX bbox;
	
	//5. Temporal
		
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass=String.class)
	private List<String> paroleChiaveLibere;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass=String.class)
	private List<String> paroleChiaveICCD;
	
	//6. Quality
	@Type(type="text")
	private String valutazioneQualita;
	
	@Type(type="text")
	private String metodoRaccoltaDati;
	
	private String scalaAcquisizione;
	
	//8. Responsabili
	@LazyCollection(LazyCollectionOption.FALSE)
	@ElementCollection(targetClass=String.class)
	private List<String> authors;

	private String responsabile;
	
	
	
	
	@Override
	public ValidationReport validateForInsertion() {
		ValidationReport toReturn=super.validateForInsertion();
		toReturn.setObjectName("Layer Concessione");
		toReturn.checkMandatory(abstractSection, "Abstract");
		toReturn.checkMandatory(subTopic, "Categoria (Topic)");
		//TODO
//		toReturn.checkMandatory(bbox, "Bounding Box");
		
		toReturn.checkMandatory(valutazioneQualita, "Valutazione della qualita");
		toReturn.checkMandatory(metodoRaccoltaDati, "Metodo raccolta dati");
		toReturn.checkMandatory(scalaAcquisizione, "Scala acquisizione");
		toReturn.checkMandatory(authors, "Autori");
		return toReturn;
	}
	
//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = super.hashCode();
//		result = prime * result + ((abstractSection == null) ? 0 : abstractSection.hashCode());
////		result = prime * result + ((authors == null) ? 0 : authors.hashCode());
//		result = prime * result + CollectionsUtils.hashCode(authors);
//		
//		
//		result = prime * result + ((bbox == null) ? 0 : bbox.hashCode());
//		result = prime * result + ((metodoRaccoltaDati == null) ? 0 : metodoRaccoltaDati.hashCode());
//		result = prime * result + ((scalaAcquisizione == null) ? 0 : scalaAcquisizione.hashCode());
//		result = prime * result + ((subTopic == null) ? 0 : subTopic.hashCode());
//		result = prime * result + ((topicCategory == null) ? 0 : topicCategory.hashCode());
//		result = prime * result + ((valutazioneQualita == null) ? 0 : valutazioneQualita.hashCode());
//		return result;
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (!super.equals(obj))
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
//		LayerConcessione other = (LayerConcessione) obj;
//		if (abstractSection == null) {
//			if (other.abstractSection != null)
//				return false;
//		} else if (!abstractSection.equals(other.abstractSection))
//			return false;
////		if (authors == null) {
////			if (other.authors != null)
////				return false;
////		} else if (!authors.equals(other.authors))
////			return false;
//		if(!CollectionsUtils.equalsCollections(authors, other.authors)) return false;
//		
//		
//		if (bbox == null) {
//			if (other.bbox != null)
//				return false;
//		} else if (!bbox.equals(other.bbox))
//			return false;
//		if (metodoRaccoltaDati == null) {
//			if (other.metodoRaccoltaDati != null)
//				return false;
//		} else if (!metodoRaccoltaDati.equals(other.metodoRaccoltaDati))
//			return false;
//		if (scalaAcquisizione == null) {
//			if (other.scalaAcquisizione != null)
//				return false;
//		} else if (!scalaAcquisizione.equals(other.scalaAcquisizione))
//			return false;
//		if (subTopic == null) {
//			if (other.subTopic != null)
//				return false;
//		} else if (!subTopic.equals(other.subTopic))
//			return false;
//		if (topicCategory == null) {
//			if (other.topicCategory != null)
//				return false;
//		} else if (!topicCategory.equals(other.topicCategory))
//			return false;
//		if (valutazioneQualita == null) {
//			if (other.valutazioneQualita != null)
//				return false;
//		} else if (!valutazioneQualita.equals(other.valutazioneQualita))
//			return false;
//		return true;
//	}
//	
	
	
}
