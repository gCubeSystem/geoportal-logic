package org.gcube.application.geoportal.model.gis;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class BBOX implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -159414992596542946L;

	public static BBOX WORLD_EXTENT=new BBOX(90d,180d,-90d,-180d);

	@NonNull
	private Double maxLat;
	@NonNull
	private Double maxLong;
	@NonNull
	private Double minLat;
	@NonNull
	private Double minLong;

	
}