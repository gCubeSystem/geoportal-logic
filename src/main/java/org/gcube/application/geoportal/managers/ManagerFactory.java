package org.gcube.application.geoportal.managers;

import java.util.Collection;

import org.gcube.application.geoportal.model.Record;
import org.gcube.application.geoportal.model.concessioni.Concessione;

public class ManagerFactory {

	
	public static <T extends Record,E extends AbstractRecordManager<T>> E getByRecord(T record) {
		if(record instanceof Concessione)
			return (E) new ConcessioneManager((Concessione)record);
		else throw new RuntimeException("Invalid record "+record);
	}
	
	
	public static <T extends Record,E extends AbstractRecordManager<T>> E getByRecordID(long id) {
		T record=(T) AbstractRecordManager.getByID(id);
		return getByRecord(record);
	}
	
	public static <T extends Record,E extends AbstractRecordManager<T>> E registerNew(T toRegister) {
		return getByRecord(toRegister);
	}
	
	public static Collection<Record> getList(){
		return AbstractRecordManager.getList();
	}
	
	public static <T extends Record> Collection<T> getList(Class<T> clazz){
		return AbstractRecordManager.getListByClass(clazz);
	}
}
