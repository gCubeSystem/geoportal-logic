package org.gcube.application.geoportal.managers;

import javax.persistence.EntityManagerFactory;

public interface EMFProvider {
	public EntityManagerFactory getFactory();
	public void shutdown();
}